from datetime import datetime

from flask import abort, flash, Flask, make_response, render_template, redirect, request, session, url_for

from . import main
from .forms import NameForm
from .. import db
from ..models import User


@main.route('/')
def index():
    user = User.query.get(session.get('id'))
    if user is not None:
        flash(f'Welcome, {user}!')
    return render_template(
        'index.html', 
        title='Main',
        current_time=datetime.utcnow(),
        name=user
    )


@main.route('/user/<name>')
def user(name):
    user = User.query.filter_by(username=name).first()
    if user is None:
        return redirect(url_for('.login'))
    return render_template('user.html', title='User', name=user)

