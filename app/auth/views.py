from flask import render_template, redirect, request, url_for, flash
from flask_login import current_user, login_required, login_user, logout_user

from app import db
from . import auth
from .forms import LoginForm, RegistrationForm
from ..email import send_mail
from ..models import User, Role


@auth.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        role_user = Role.query.filter_by(name='User').first()
        user = User(
            email=form.email.data,
            username=form.username.data,
            password=form.password.data,
            role=role_user
        )
        db.session.add(user)
        db.session.commit()
        token = user.generate_confirmation_token()
        send_mail(
            user.email,
            'Confirm Your Account',
            'auth/email/confirm',
            user=user,
            token=token
        )
        flash('You can now login')
        return redirect(url_for('auth.login'))
    return render_template('auth/register.html', title='Sign up', form=form)


@auth.route('/confirm/<token>')
@login_required
def confirm(token):
    if not current_user.confirmed:
        if current_user.confirm(token):
            flash('You have confirmed your account. Thanks!')
        else:
            flash('The confirmation link is invalid or or has expired')
    return redirect(url_for('main.index'))


@auth.route('/confirm')
@login_required
def resend_confirmation():
    token = current_user.generate_confirmation_token()
    send_mail(
        current_user.email,
        'Confirm Your Account',
        'auth/email/confirm',
        user=current_user,
        token=token
    )
    flash('A new confirmation email has been sent to you by email.')
    return redirect(url_for('main.index'))


@auth.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is not None and user.verify_password(form.password.data):
            login_user(user, form.remember_me.data)
            return redirect(request.args.get('next') or url_for('main.index'))
        flash('Invalid email or password')
    return render_template('auth/login.html', title='Sign in', form=form)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You have been logged out')
    return redirect(url_for('main.index'))


@auth.before_app_request
def before_request():
    if current_user.is_authenticated:
        # current_user.ping()
        if not current_user.confirmed \
                and request.endpoint \
                and request.blueprint != 'auth' \
                and request.endpoint != 'static':
            return redirect(url_for('auth.unconfirmed'))

@auth.route('/unconfirmed')
def unconfirmed():
    if current_user.is_anonymous or current_user.confirmed:
        return redirect(url_for('main.index'))
    return render_template('auth/unconfirmed.html', title='Confirm Your account')
