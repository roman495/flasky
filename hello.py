from datetime import datetime
from threading import Thread
import os

from flask import abort, flash, Flask, make_response, render_template, redirect, request, session, url_for

from flask_mail import Mail, Message
from flask_migrate import Migrate, MigrateCommand
from flask_moment import Moment
from flask_script import Manager, Shell
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class Config:
    SECRET_KEY = 'adsfsd89l2h8al39gs32h9av9'
    SQLALCHEMY_DATABASE_URI = 'mysql://root@localhost/flasky'
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    MAIL_SERVER = 'smtp.mailtrap.io'
    MAIL_PORT = '25'
    MAIL_USE_TLS = True
    MAIL_USERNAME = '7c6aa0331c9f89'
    MAIL_PASSWORD = '0dbcdcd98cbf2a'
    FLASKY_MAIL_SUBJECT_PREFIX = '[Flasky]'
    FLASKY_MAIL_SENDER = 'Flasky Admin <flasky@example.com>'
    FLASKY_ADMIN = 'admin@example.com'
    

app = Flask(__name__)
app.config.from_object(Config())

db = SQLAlchemy(app)
manager = Manager(app)
mail = Mail(app)
migrate = Migrate(app, db)
moment = Moment(app)

manager.add_command('db', MigrateCommand)

def send_mail_async(app, msg):
    with app.app_context():
        mail.send(msg)


def send_mail(to, subject, template, **kwargs):
    msg = Message(
        app.config['FLASKY_MAIL_SUBJECT_PREFIX'] + subject,
        sender=app.config['FLASKY_MAIL_SENDER'],
        recipients=[to]
    )
    msg.body = render_template(template + '.txt', **kwargs)
    msg.html = render_template(template + '.html', **kwargs)
    thr = Thread(target=send_mail_async, args=[app, msg])
    thr.start()
    return thr


class NameForm(FlaskForm):
    name = StringField('What is your name?', validators=[DataRequired()])


class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    users = db.relationship('User', backref='role', lazy='dynamic')
    
    def __repr__(self):
        return '<Role %r>' % self.name


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True, index=True)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))

    def __repr__(self):
        return '<User %r>' % self.username
        
    def __str__(self):
        return self.username


@app.route('/')
def index():
    user = User.query.get(session.get('id'))
    if user is not None:
        flash(f'Welcome, {user}!')
    return render_template(
        'index.html', 
        title='Main',
        current_time=datetime.utcnow(),
        name=user
    )


@app.route('/login', methods=['GET', 'POST'])
def login():
    name = None
    form = NameForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.name.data).first()
        if user is None:
            user_role = Role.query.filter_by(name='user').first()
            user = User(username=form.name.data, role=user_role)
            db.session.add(user)
            send_mail(
                app.config['FLASKY_ADMIN'],
                'New user',
                'mail/new_user',
                user=user
            )
        
        session['id'] = user.id
        form.name.data = ''
        return redirect(url_for('index'))
    return render_template('login.html', title='Log in', form=form)


@app.route('/user/<name>')
def user(name):
    user = User.query.filter_by(username=name).first()
    if user is None:
        return redirect(url_for('login'))
    return render_template('user.html', title='User', name=user)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html', title='Page not found'), 404
    
    
@app.errorhandler(500)
def internal_server_error(e):
    return render_template('500.html', title='Internal server error'), 500


@app.shell_context_processor
def make_shell_context():
    return dict(db=db, User=User, Role=Role)
    

if __name__ == '__main__':
    manager.run()
